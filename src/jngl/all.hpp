// Copyright 2012-2020 Jan Niklas Hasse <jhasse@bixense.com>
// For conditions of distribution and use, see copyright notice in LICENSE.txt
/// @file
/// Includes all JNGL headers
#pragma once

#include "App.hpp"
#include "Color.hpp"
#include "Controller.hpp"
#include "Shader.hpp"
#include "ShaderProgram.hpp"
#include "Vertex.hpp"
#include "Video.hpp"
#include "debug.hpp"
#include "drawable.hpp"
#include "font.hpp"
#include "framebuffer.hpp"
#include "input.hpp"
#include "job.hpp"
#include "main.hpp"
#include "matrix.hpp"
#include "message.hpp"
#include "other.hpp"
#include "screen.hpp"
#include "shapes.hpp"
#include "sound.hpp"
#include "sprite.hpp"
#include "text.hpp"
#include "time.hpp"
#include "window.hpp"
#include "work.hpp"
