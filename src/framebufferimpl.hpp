// Copyright 2011-2020 Jan Niklas Hasse <jhasse@bixense.com>
// For conditions of distribution and use, see copyright notice in LICENSE.txt

#pragma once

#include "texture.hpp"

namespace jngl {

class FrameBufferImpl {
public:
	FrameBufferImpl(int width, int height);
	FrameBufferImpl(const FrameBufferImpl&) = delete;
	FrameBufferImpl& operator=(const FrameBufferImpl&) = delete;
	FrameBufferImpl(FrameBufferImpl&&) = delete;
	FrameBufferImpl& operator=(FrameBufferImpl&&) = delete;
	~FrameBufferImpl();
	void BeginDraw();
	void EndDraw();
	void Draw(double x, double y) const;
	static void clear();

private:
	GLuint fbo = 0;
	GLuint buffer = 0;
	const int height;
	Texture texture;
	bool letterboxing;
	GLuint systemFbo;
	GLuint systemBuffer;
#ifndef GL_VIEWPORT_BIT
	GLint viewport[4];
#endif
};

} // namespace jngl
