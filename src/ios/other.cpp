// Copyright 2020 Jan Niklas Hasse <jhasse@bixense.com>
// For conditions of distribution and use, see copyright notice in LICENSE.txt

#include "../jngl/other.hpp"
#include "../jngl/Controller.hpp"

namespace jngl {

std::string getBinaryPath() {
	return "";
}

std::vector<std::shared_ptr<Controller>> getConnectedControllers() {
	return {};
}

} // namespace jngl
